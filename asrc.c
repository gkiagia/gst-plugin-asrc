#include "config.h"
#include <gst/audio/audio.h>

GST_DEBUG_CATEGORY_STATIC (asrcdebug);
#define GST_CAT_DEFAULT asrcdebug

G_DECLARE_FINAL_TYPE (GstAsrc, gst_asrc, GST, ASRC, GstAudioFilter)

struct _GstAsrc
{
  GstAudioFilter parent;

  GstAudioInfo *info;

  GstClockTime last_pts;
  gint actual_frames;

  GstAudioResampler *resampler;
};

#define gst_asrc_parent_class parent_class
G_DEFINE_TYPE (GstAsrc, gst_asrc, GST_TYPE_AUDIO_FILTER)

#define SUPPORTED_FORMATS "{ " \
  GST_AUDIO_NE(S16) ", " \
  GST_AUDIO_NE(S32) ", " \
  GST_AUDIO_NE(F32) ", " \
  GST_AUDIO_NE(F64) " }"

static GstStaticPadTemplate sink_template = GST_STATIC_PAD_TEMPLATE (
  "sink",
  GST_PAD_SINK,
  GST_PAD_ALWAYS,
  GST_STATIC_CAPS (GST_AUDIO_CAPS_MAKE (SUPPORTED_FORMATS))
);

static GstStaticPadTemplate src_template = GST_STATIC_PAD_TEMPLATE (
  "src",
  GST_PAD_SRC,
  GST_PAD_ALWAYS,
  GST_STATIC_CAPS (GST_AUDIO_CAPS_MAKE (SUPPORTED_FORMATS))
);

static void
gst_asrc_init (GstAsrc *self)
{
}

static gboolean
gst_asrc_setup (GstAudioFilter * filter, const GstAudioInfo * info)
{
  GstAsrc *self = GST_ASRC (filter);
  GstStructure *options;

  self->last_pts = GST_CLOCK_TIME_NONE;

  g_clear_pointer (&self->info, gst_audio_info_free);
  self->info = gst_audio_info_copy (info);

  GstAudioResamplerFlags flags = GST_AUDIO_RESAMPLER_FLAG_VARIABLE_RATE;
  // if (GST_AUDIO_INFO_LAYOUT (info) == GST_AUDIO_LAYOUT_NON_INTERLEAVED) {
  //   flags |= GST_AUDIO_RESAMPLER_FLAG_NON_INTERLEAVED_IN;
  //   flags |= GST_AUDIO_RESAMPLER_FLAG_NON_INTERLEAVED_OUT;
  // }

  options = gst_structure_new_empty ("GstAudioResampler.options");
  gst_audio_resampler_options_set_quality (
      GST_AUDIO_RESAMPLER_METHOD_KAISER,
      GST_AUDIO_RESAMPLER_QUALITY_DEFAULT,
      GST_AUDIO_INFO_RATE (info),
      GST_AUDIO_INFO_RATE (info),
      options);

  g_clear_pointer (&self->resampler, gst_audio_resampler_free);
  self->resampler = gst_audio_resampler_new (
      GST_AUDIO_RESAMPLER_METHOD_KAISER,
      flags,
      GST_AUDIO_INFO_FORMAT (info),
      GST_AUDIO_INFO_CHANNELS (info),
      GST_AUDIO_INFO_RATE (info),
      GST_AUDIO_INFO_RATE (info),
      options);

  gst_structure_free (options);

  return TRUE;
}

static gboolean
gst_asrc_stop (GstBaseTransform *trans)
{
  GstAsrc *self = GST_ASRC (trans);
  g_clear_pointer (&self->info, gst_audio_info_free);
  g_clear_pointer (&self->resampler, gst_audio_resampler_free);
  return TRUE;
}

static gboolean
gst_asrc_transform_size (GstBaseTransform *trans,
    GstPadDirection direction,
    GstCaps *caps, gsize size,
    GstCaps *othercaps, gsize *othersize)
{
  GstAsrc *self = GST_ASRC (trans);

  gsize frames = size / GST_AUDIO_INFO_BPF (self->info);
  gsize res_frames;

  if (direction == GST_PAD_SINK)
    res_frames = gst_audio_resampler_get_out_frames (self->resampler, frames);
  else
    res_frames = gst_audio_resampler_get_in_frames (self->resampler, frames);

  GST_DEBUG_OBJECT (self, "in: %" G_GSIZE_FORMAT ", out: %" G_GSIZE_FORMAT,
      frames, res_frames);

  *othersize = res_frames * GST_AUDIO_INFO_BPF (self->info);
  return TRUE;
}

static GstFlowReturn
do_resample (GstAsrc *self, GstBuffer *inbuf, GstBuffer *outbuf)
{
  GstMapInfo inmap, outmap;
  gpointer inframes[1], outframes[1];

  gsize n_in_frames =
      gst_buffer_get_size (inbuf) / GST_AUDIO_INFO_BPF (self->info);
  gsize n_out_frames =
      gst_audio_resampler_get_out_frames (self->resampler, n_in_frames);

  gst_buffer_set_size (outbuf, n_out_frames * GST_AUDIO_INFO_BPF (self->info));
  GST_BUFFER_DURATION (outbuf) =
      GST_FRAMES_TO_CLOCK_TIME (n_out_frames, GST_AUDIO_INFO_RATE (self->info));

  if (!gst_buffer_map (inbuf, &inmap, GST_MAP_READ))
    return GST_FLOW_ERROR;

  if (!gst_buffer_map (outbuf, &outmap, GST_MAP_READWRITE)) {
    gst_buffer_unmap (inbuf, &inmap);
    return GST_FLOW_ERROR;
  }

  inframes[0] = inmap.data;
  outframes[0] = outmap.data;
  gst_audio_resampler_resample (self->resampler,
      inframes, n_in_frames, outframes, n_out_frames);

  gst_buffer_unmap (inbuf, &inmap);
  gst_buffer_unmap (outbuf, &outmap);

  return GST_FLOW_OK;
}

static GstFlowReturn
gst_asrc_transform (GstBaseTransform *trans, GstBuffer *inbuf, GstBuffer *outbuf)
{
  GstAsrc *self = GST_ASRC (trans);
  GstFlowReturn ret = GST_FLOW_OK;
  gdouble corr = 1.0;

  if (self->last_pts != GST_CLOCK_TIME_NONE && !GST_BUFFER_IS_DISCONT (inbuf)) {
    GstClockTime calibrated_dur;
    gint calibrated_frames;

    calibrated_dur = GST_BUFFER_PTS (inbuf) - self->last_pts;
    calibrated_frames = GST_CLOCK_TIME_TO_FRAMES (calibrated_dur,
        GST_AUDIO_INFO_RATE (self->info));

    corr = (gdouble) self->actual_frames / (gdouble) calibrated_frames;

    GST_INFO_OBJECT (self, "corr: %lf, calibrated: %d, exp: %d",
        corr, calibrated_frames, self->actual_frames);
  }

  ret = do_resample (self, inbuf, outbuf);

  gst_audio_resampler_update (self->resampler,
      GST_AUDIO_INFO_RATE (self->info) * corr,
      GST_AUDIO_INFO_RATE (self->info),
      NULL);

  self->last_pts = GST_BUFFER_PTS (inbuf);
  self->actual_frames = GST_CLOCK_TIME_TO_FRAMES (
      GST_BUFFER_DURATION (inbuf),
      GST_AUDIO_INFO_RATE (self->info));
  return ret;
}

static void
gst_asrc_class_init (GstAsrcClass *class)
{
  GstElementClass *eclass = GST_ELEMENT_CLASS (class);
  GstBaseTransformClass *btclass = GST_BASE_TRANSFORM_CLASS (class);
  GstAudioFilterClass *afclass = GST_AUDIO_FILTER_CLASS (class);

  gst_element_class_set_static_metadata (eclass,
      "GstAsrc",
      "Filter/Effect/Audio",
      "Adaptive Stream Rate Control",
      "George Kiagiadakis <george.kiagiadakis@collabora.com>");

  gst_element_class_add_static_pad_template (eclass, &sink_template);
  gst_element_class_add_static_pad_template (eclass, &src_template);

  afclass->setup = gst_asrc_setup;
  btclass->stop = gst_asrc_stop;
  btclass->transform_size = gst_asrc_transform_size;
  btclass->transform = gst_asrc_transform;

  GST_DEBUG_CATEGORY_INIT (asrcdebug, "asrc", 0, "Asrc element debug");
}

static gboolean
plugin_init (GstPlugin *plugin)
{
  if (!gst_element_register (plugin, "asrc", GST_RANK_NONE,
          gst_asrc_get_type ()))
    return FALSE;

  return TRUE;
}

GST_PLUGIN_DEFINE (
  GST_VERSION_MAJOR,
  GST_VERSION_MINOR,
  asrcplugin,
  "ASRC plugin",
  plugin_init,
  VERSION,
  "LGPL-2.1",
  "asrcplugin",
  "http://gstreamer.net/no-url-yet"
)
